import React from 'react'

interface IconProps {
	size?: number
	fill?: string
}

export const WarrenIcon = ({ size = 28, fill }: IconProps) => {
	return (
		<svg className="svg-icon svg-fill" viewBox="0 0 48 47" width={size} height={size}>
			<path
				fill={fill}
				fillRule="evenodd"
				d="M36 29.55c0 2.538-2.05 4.595-4.578 4.595a.962.962 0 01-.96-.964V13.007c0-.532.43-.964.96-.964h3.618c.53 0 .96.432.96.964v16.544zm-9.23 3.631c0 .533-.43.964-.961.964H22.19a.962.962 0 01-.96-.964V18.533c0-.532.43-.964.96-.964h3.618c.53 0 .96.432.96.964v14.648zm-9.232 0c0 .533-.43.964-.96.964H12.96a.962.962 0 01-.961-.964V15.787c0-.533.43-.964.96-.964h3.618c.53 0 .96.431.96.964V33.18zM30.461 0H17.538C7.852 0 0 7.88 0 17.602v27.623c0 .532.43.964.96.964h29.501C40.148 46.189 48 38.308 48 28.587V17.602C48 7.881 40.148 0 30.461 0z"
			></path>
		</svg>
	)
}
