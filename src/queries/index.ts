// Packages
import axios from 'axios'

const API = process.env.REACT_APP_API || ''

const getAnswers = async (key: string) => {
	const { data } = await axios.get(`${API}/${key}`)
	return data
}

const postMessage = async (key: string, params?: Record<string, unknown>) => {
	const { data } = await axios({
		method: 'post',
		url: `${API}/${key}`,
		data: {
			...params
		}
	})

	return data
}

const api = {
	getAnswers,
	postMessage
}

export default api
