// Packages
import React from 'react'

// Pages
import Suitability from './Suitability'

const App = () => <Suitability />

export default App
