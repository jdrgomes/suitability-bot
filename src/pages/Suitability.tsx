// Packages
import React, { useState, useEffect } from 'react'
import { useMutation } from 'react-query'
import ReactLoading from 'react-loading'
import AutoScroll from '@brianmcallister/react-auto-scroll'

// Queries
import api from '../queries'

// Contants
import { CONVERSATION, FINISH } from '../constants'

// Types
import { ElementsProps, MessageType } from '../types'

// Helpers
import { inputIdVerifier, normalizeAmount } from '../helpers'

// Components
import { SuitabilityHeader, InputForm, Button, Message, UserMessage } from '../components'

// Styles
import {
	Container,
	Footer,
	FooterContainer,
	FooterGrid,
	FooterGridFlex,
	FooterStart,
	FooterEnd,
	FooterProfile,
	FooterProfileText,
	FooterLoading
} from '../styles'

const Suitability = () => {
	const [data, setData] = useState<Record<string, unknown>>({})
	const [elements, setElements] = useState<ElementsProps>({ inputs: [], buttons: [] })
	const [messages, setMessages] = useState<MessageType[][]>([])
	const [id, setId] = useState('')
	const [answers, setAnswers] = useState({})
	const [finalAnswers, setFinalAnswers] = useState({})
	const [value, setValue] = useState<string | number>('')
	const [profile, setProfile] = useState('')
	const [showUserAnswer, setShowUserAnswer] = useState<string[]>([])

	const placeType = inputIdVerifier(id)
	const [messagesMutate] = useMutation((params: Record<string, unknown>) => api.postMessage(CONVERSATION, params))

	const [finishMutate, { isLoading }] = useMutation((params: Record<string, unknown>) =>
		api.postMessage(FINISH, params)
	)

	const postConversation = async () => {
		try {
			const data = await messagesMutate({ context: 'suitability', ...answers })
			setData(data)
			setId(data.id)
			setElements({ inputs: data.inputs, buttons: data.buttons })
			setMessages(messages => [...messages, data.messages])

			verifyIsConversationEnded(data.id)
		} catch (error) {
			console.error(error)
		}
	}

	const sendfinalAnswers = async () => {
		try {
			const {
				user: { investmentProfile }
			} = await finishMutate({ answers: { ...finalAnswers } })
			setProfile(investmentProfile.riskToleranceProfile)
		} catch (error) {
			console.error(error)
		}
	}

	const verifyIsConversationEnded = (id: string) => {
		if (id === 'final') {
			sendfinalAnswers()
		}
	}

	const handleChange = (answer: string | number) => setValue(answer)

	const handleSubmit = (params: string | number, buttonLabel?: string) => {
		const { id } = data as { id: string }
		const valueInputed = elements.inputs.length === 0 ? params : value
		const answerValue = id === 'question_income' ? normalizeAmount(String(valueInputed)) : valueInputed

		const payload = {
			id,
			answers: { [id]: answerValue, answers }
		}

		setFinalAnswers({ id, [id]: answerValue, ...finalAnswers })
		setAnswers(payload)
		setValue('')
		setShowUserAnswer(userAnswers => [...userAnswers, String(buttonLabel || valueInputed)])
	}

	useEffect(() => {
		postConversation()
	}, [answers])

	return (
		<>
			<SuitabilityHeader />

			<AutoScroll showOption={false} height={620}>
				<Container>
					{messages.map((item, index) => (
						<div key={index}>
							<Message messages={item} />
							<UserMessage answer={showUserAnswer[index]} />
						</div>
					))}
					{isLoading && (
						<FooterLoading>
							<ReactLoading type="cylon" color="#c9dbe8" height={120} width={120} />
						</FooterLoading>
					)}

					{profile && (
						<FooterProfile>
							<FooterProfileText>{profile}</FooterProfileText>
						</FooterProfile>
					)}
				</Container>
			</AutoScroll>

			{!isLoading && !profile && (
				<Footer>
					<FooterContainer>
						<form>
							{elements && elements.inputs.length !== 0 && (
								<FooterGrid>
									<FooterStart>
										<InputForm
											type={placeType.type}
											mask={placeType.mask}
											placeholder={placeType.text}
											value={value}
											onChange={handleChange}
										/>
									</FooterStart>
									<FooterEnd>
										<Button text="Ok" onClick={() => handleSubmit(value)} />
									</FooterEnd>
								</FooterGrid>
							)}

							{elements && elements.buttons.length !== 0 && (
								<FooterGridFlex>
									{elements.buttons.map(btn => (
										<Button
											key={btn.value}
											text={btn.label.title}
											onClick={() => handleSubmit(btn.value, btn.label.title)}
										/>
									))}
								</FooterGridFlex>
							)}
						</form>
					</FooterContainer>
				</Footer>
			)}
		</>
	)
}

export default Suitability
