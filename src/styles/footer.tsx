import styled from 'styled-components'
import media from 'styled-media-query'

// Theme
import { theme } from '../theme'

export const Footer = styled.footer`
	width: 100%;
	position: fixed;
	left: 0px;
	bottom: 0px;
	padding: 20px;
	background-color: ${theme.colors.flatBlue};
`

export const FooterContainer = styled.div`
	max-width: 960px;
	margin: 0 auto;
`

export const FooterGrid = styled.div`
	grid-template-columns: repeat(2, 1fr);
	-webkit-box-align: center;
	align-items: center;
	-webkit-box-pack: justify;
	justify-content: space-between;
	display: grid;
`

export const FooterGridFlex = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: row;
`

export const FooterStart = styled.div`
	justify-self: flex-start;
	justify-content: flex-start;
`

export const FooterEnd = styled.div`
	justify-self: flex-end;
	justify-content: flex-end;
`

export const FooterProfile = styled.div`
	background-color: ${theme.colors.semiDarkBlue};
	padding: ${theme.spaces.medium};
	text-align: center;
	border-radius: 20px;
`

export const FooterProfileText = styled.h1`
	color: ${theme.colors.white};
	font-size: ${theme.fontSizes.large};
	font-weight: ${theme.fontWeights.bold};
	text-transform: uppercase;

	${media.lessThan('small')`
	font-size: ${theme.fontSizes.normal};
  `}
`

export const FooterLoading = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
`
