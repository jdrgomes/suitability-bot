import styled from 'styled-components'
import { theme } from '../theme'

export const Hero = styled.section`
	height: 100%;
	overflow-y: none;
`

export const HomeContent = styled.section`
	min-height: 100vh;
	display: flex;
	justify-content: center;
	align-items: center;
`

export const HomeContentItem = styled.div`
	font-size: 64px;
	font-weight: ${theme.fontWeights.bold};

	h4 {
		font-size: 1.3rem;
		line-height: 1.5em;
		font-weight: ${theme.fontWeights.body};
		margin-top: 10px;
		width: 460px;
	}

	button {
		cursor: pointer;
		white-space: nowrap;
		text-align: center;
		transition: all 0.3s ease 0s;
		overflow: hidden;
		border-width: 2px;
		border-style: solid;
		text-decoration: none;
		font: 600 20px / 35px Inter, sans-serif;
		display: inline-block;
		background-color: #72d9b4;
		color: rgb(255, 255, 255);
		border-color: #72d9b4;
		padding: 12px 46px;
		border-radius: 32px;
	}
`
