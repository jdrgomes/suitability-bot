import styled from 'styled-components'
import media from 'styled-media-query'

export const Container = styled.div`
	max-width: 960px;
	margin: 0 auto;
	padding: 0 24px;
	width: 100%;
	padding: 0 24px;

	${media.lessThan('medium')`
		max-width: 465px;
		padding-bottom: 80px;
  `}
`
