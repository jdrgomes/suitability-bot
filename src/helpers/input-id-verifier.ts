interface inputPlaceTypeProp {
	text: string
	type: string
	mask?: string
}

export const inputIdVerifier = (id: string) => {
	let inputPlaceType: inputPlaceTypeProp = { text: '', type: '', mask: '' }

	switch (id) {
		case 'question_name':
			return (inputPlaceType = { text: 'Digite seu nome', type: 'text' })
		case 'question_age':
			return (inputPlaceType = { text: 'Informe a sua idade', type: 'number', mask: 'number' })
		case 'question_income':
			return (inputPlaceType = { text: 'Informe a sua renda', type: 'text', mask: 'money' })
		case 'question_email':
			return (inputPlaceType = { text: 'Qual o seu email?', type: 'email' })
		default:
			break
	}

	return inputPlaceType
}
