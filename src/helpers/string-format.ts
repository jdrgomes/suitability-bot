// Packages
import Intl from 'intl'
import 'intl/locale-data/jsonp/pt'

export const currency = (amount = 0, hideSymbol?: boolean) => {
	const isNegative = amount < 0
	const sign = isNegative ? '- ' : ''
	const positiveAmount = Math.abs(amount)
	const normalizedAmount = positiveAmount / 100

	const localizedAmount = new Intl.NumberFormat('pt-BR', {
		currency: 'BRL',
		minimumFractionDigits: 2
	}).format(normalizedAmount)

	return `${sign}${hideSymbol ? '' : 'R$ '}${localizedAmount}`
}

export const onlyNumbers = (str: string): string => str.replace(/\D/g, '')

export const unmask = (string: string) =>
	string
		.replace(/\./g, '')
		.replace(/-/g, '')
		.replace(/\)/g, '')
		.replace(/\(/g, '')
		.replace(/\//g, '')
		.replace(/\s/g, '')
		.replace(/_/g, '')

export const stringToCurrency = (amount: string): number => {
	if (amount === '') return 0
	const withoutPrefix = amount.replace('R$', '')
	const [integer, fraction] = withoutPrefix.split(',')
	const formatedInteger = integer.split('.').join('')
	return +[formatedInteger, fraction].join('.')
}

export const normalizeAmount = (amount: string): number => {
	return Number(Number(stringToCurrency(amount)).toFixed(2).replace(/\D/g, ''))
}
