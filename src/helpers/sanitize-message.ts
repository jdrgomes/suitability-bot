export const sanitizeMessage = (message: string) => {
	const splitText = message.split('^')
	let accumalator = 0

	const normalizedText = splitText.map(text => {
		const sentence = text.replace(/[0-9]/g, '')
		const delay = text.replace(/\D/g, '')

		accumalator += Number(delay)

		return {
			sentence: sentence.trim(),
			delay: delay && accumalator
		}
	})

	return normalizedText
}

// retornar a versao anterior enviando apernas o texto e o delay
