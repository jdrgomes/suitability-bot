// Packages
import React from 'react'
import ReactDOM from 'react-dom'

// Pages
import App from '../src/pages/App'

// Theme
import { GlobalStyle } from './theme'

ReactDOM.render(
	<>
		<GlobalStyle />
		<App />
	</>,
	document.getElementById('root')
)
