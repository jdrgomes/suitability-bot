import styled from 'styled-components'
import media from 'styled-media-query'

// Theme
import { theme } from '../../theme'

export const MessageBlock = styled.div`
	display: flex;
	flex-direction: row;
	padding-top: 40px;
`

export const MessageContent = styled.div`
	margin-left: 16px;
`

export const MessageIconBlock = styled.span`
	display: block;
	width: 40px;
	margin-right: 20px;
`

export const MessageTyping = styled.p`
	align-self: flex-start;
	background: ${theme.colors.semiDarkBlue};
	font-size: ${theme.fontSizes.normal};
	margin-bottom: 15px;
	padding: 10px 15px;
	border-radius: 20px;
	border-top-left-radius: 2px;

	${media.lessThan('medium')`
		${theme.fontSizes.tiny}
	`}
`
