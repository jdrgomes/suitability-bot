// Packages
import React from 'react'

// Icons
import { WarrenIcon } from '../../icons'

// Types
import { MessageType } from '../../types'

// Styles
import { MessageBlock, MessageContent, MessageIconBlock, MessageTyping } from './styles'

interface MessageProps {
	messages: MessageType[]
}

export const Message = ({ messages }: MessageProps) => {
	const messageAndDelay: { sentence: string }[] = []

	messages.map(item => {
		const { value } = item
		const cleanText = value.replace(/\^\d+/gm, '').replace(/<erase>/gm, '')

		messageAndDelay.push({ sentence: cleanText.trim() })
	})

	return (
		<>
			<MessageBlock>
				<MessageIconBlock>
					<WarrenIcon fill="#80c8ff" />
				</MessageIconBlock>

				<MessageContent>
					{messages.length > 0 &&
						messageAndDelay.map(({ sentence }, index) => (
							<div key={index}>
								<MessageTyping>{sentence}</MessageTyping>
							</div>
						))}
				</MessageContent>
			</MessageBlock>
		</>
	)
}
