import styled from 'styled-components'
import media from 'styled-media-query'

// Theme
import { theme } from '../../theme'

export const InputWrapper = styled.input`
	width: 100%;
	border: none;
	background-color: inherit;
	font-family: Inter;
	font-size: ${theme.fontSizes.xLarge};
	font-weight: ${theme.fontWeights.body};
	color: ${theme.colors.white};

	${media.lessThan('medium')`
		font-size: ${theme.fontSizes.medium};
  `}

	${media.lessThan('small')`
		font-size: ${theme.fontSizes.normal};
  `}

	&:focus {
		outline: none;
	}

	&::placeholder {
		color: ${theme.colors.semiDarkBlue};
	}

	&::-webkit-outer-spin-button,
	&::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}

	input[type='number'] {
		-moz-appearance: textfield;
	}
`
