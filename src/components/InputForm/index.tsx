// Packages
import React from 'react'

// Helpers
import { onlyNumbers, currency } from '../../helpers'

// Styles
import { InputWrapper } from './styles'

interface InputFormProps {
	type?: string
	mask?: string
	placeholder?: string
	value: string | number
	onChange: (value: string | number) => void
}

export const InputForm = ({ type, mask, placeholder, onChange }: InputFormProps) => {
	return (
		<InputWrapper
			type={type}
			placeholder={placeholder}
			onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
				if (mask === 'money' || mask === 'number') {
					const val = +onlyNumbers(e.target.value)
					const valueMasked = mask === 'money' ? currency(val) : val
					e.target.value = String(valueMasked)

					return onChange(valueMasked)
				}

				return onChange(e.target.value)
			}}
		/>
	)
}
