import styled from 'styled-components'
import media from 'styled-media-query'

// Theme
import { theme } from '../../theme'

export const StyledButton = styled.button`
	height: 50px;
	border-radius: 50px;
	border: none;
	padding: 0px 40px;
	margin: 6px 6px;
	box-shadow: 0px 2px 8px rgba(46, 45, 51, 0.15);
	background-color: ${theme.colors.semiDarkBlue};
	font-size: ${theme.fontSizes.normal};
	color: ${theme.colors.lighterBlue};
	cursor: pointer;

	${media.lessThan('medium')`
			font-size: ${theme.fontSizes.tiny};
  `}
`
