// Packages
import React from 'react'

// Styles
import { StyledButton } from './styles'

interface ButtonProps {
	text: string
	onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void
}

export const Button = ({ text, onClick }: ButtonProps) => {
	return (
		<StyledButton type="button" onClick={onClick}>
			{text}
		</StyledButton>
	)
}
