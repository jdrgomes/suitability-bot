import styled from 'styled-components'
import media from 'styled-media-query'

// Theme
import { theme } from '../../theme'

export const UserMessageBlock = styled.div`
	display: flex;
	flex-direction: row;
	margin-bottom: 30px;
	justify-content: flex-end;
`

export const UserIconBlock = styled.span`
	display: block;
	width: 48px;
	margin-left: 20px;
`

export const UserMessageContent = styled.div`
	text-align: right;
	align-self: flex-start;
	background: ${theme.colors.semiDarkBlue};
	font-size: ${theme.fontSizes.normal};
	font-weight: ${theme.fontWeights.bold};
	margin-bottom: 15px;
	padding: 15px 20px;
	border-radius: 20px;
	border-top-right-radius: 2px;

	${media.lessThan('medium')`
		${theme.fontSizes.tiny}
	`}
`
