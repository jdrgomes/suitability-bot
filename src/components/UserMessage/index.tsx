// Packages
import React from 'react'

// Icons
import { Send } from '../../icons'

// Styles
import { UserMessageBlock, UserIconBlock, UserMessageContent } from './styles'

interface UserMessageProps {
	answer: string
}

export const UserMessage = ({ answer }: UserMessageProps) => (
	<UserMessageBlock>
		{answer && (
			<>
				<UserMessageContent>{answer}</UserMessageContent>

				<UserIconBlock>
					<Send fill="#124750" />
				</UserIconBlock>
			</>
		)}
	</UserMessageBlock>
)
