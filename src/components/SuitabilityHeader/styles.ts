import styled from 'styled-components'
import media from 'styled-media-query'

// Theme
import { theme } from '../../theme'

export const Header = styled.header`
	width: 100%;
	display: flex;
	justify-content: center;
	background-color: ${theme.colors.semiDarkBlue};
`

export const HeaderContent = styled.div`
	padding: ${theme.spaces.tiny};
	width: 1200px;
`

export const HeaderGrid = styled.div`
	grid-template-columns: repeat(1, 1fr);
	-webkit-box-align: center;
	align-items: center;
	-webkit-box-pack: justify;
	justify-content: space-between;
	padding-top: 1rem;
	padding-bottom: 1rem;
	display: grid;
`

export const HeaderTitle = styled.h1`
	justify-self: center;
	color: ${theme.colors.lighterBlue};
	font-size: ${theme.fontSizes.small};
	font-weight: ${theme.fontWeights.heading};
	text-transform: uppercase;

	${media.lessThan('small')`
		font-size: ${theme.fontSizes.tiny};
  `}
`
