// Packages
import React from 'react'

// Styles
import { Header, HeaderContent, HeaderGrid, HeaderTitle } from './styles'

const HEADER_TITLE = 'Abra sua conta'

export const SuitabilityHeader = () => (
	<Header>
		<HeaderContent>
			<HeaderGrid>
				<HeaderTitle>{HEADER_TITLE}</HeaderTitle>
			</HeaderGrid>
		</HeaderContent>
	</Header>
)
