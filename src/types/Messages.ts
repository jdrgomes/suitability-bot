export type MessageType = {
	type: string
	value: string
}
