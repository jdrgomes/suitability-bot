interface Inputs {
	mask: string
	type: string
}

interface Button {
	label: { title: string }
	value: string
}

export interface ElementsProps {
	inputs: Inputs[]
	buttons: Button[]
}
