// Packages
import { createGlobalStyle } from 'styled-components'

// Theme
import colors from './colors'

export const GlobalStyle = createGlobalStyle`
 @font-face {
    font-family: "Inter";
    font-weight: 300;
    font-style: normal;
    src: url("/fonts/Inter-Black.woff2") format("woff2"),
      url("/fonts/Inter-Black.woff") format("woff");
  }
  @font-face {
    font-family: "Inter" ;
    font-weight: 600;
    font-style: normal;
    src: url("/fonts/Inter-Medium.woff2") format("woff2"),
      url("/fonts/Inter-Medium.woff") format("woff");
  }
  @font-face {
    font-family: "Inter" ;
    font-weight: 700;
    font-style: normal;
    src: url("/fonts/Inter-Bold.woff2") format("woff2"),
      url("/fonts/Inter-Bold.woff") format("woff");
  }
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    -moz-osx-font-smoothing: grayscale;
    text-rendering: optimizeLegibility;
    text-size-adjust: none;
    font-family: Inter, Helvetica, Arial, sans-serif;
  }
  :root{
    line-height: 1.5;
    color: ${colors.lighterBlue};
    background: ${colors.primary} ;

  }
  body {
    background: ${colors.primary} ;
  }
  .debug > * {
    border: 2px solid red;
  }
`
