const brand = {
	primary: '#0d171e',
	flatBlue: '#416581',
	lighterBlue: '#c9dbe8',
	semiDarkBlue: '#111e27',
	grape: '#4e0442',
	white: '#fff'
}

export default {
	...brand
}
