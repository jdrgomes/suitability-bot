import colors from './colors'
import spaces from './spaces'

import fontSizes from './font-sizes'

export const theme = {
	name: 'Default',
	colors,
	fontSizes,
	spaces,
	fontWeights: {
		body: 400,
		heading: 700,
		bold: 700,
		medium: 500,
		light: 400
	}
}
