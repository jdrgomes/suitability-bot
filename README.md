# Suitability Bot

- React
- TypeScript
- Axios
- Styled-Components

### Setup

Para executar essa aplicação localmente, é necessário:

1. Clonar o projeto e navegar para o diretório:

```
git clone git@bitbucket.org:jdrgomes/suitability-bot.git && cd suitability-bot
```

2. Instale as dependências:

```
yarn ou npm i
```

3. Rode a aplicação:

```
yarn start ou npm run start
```

5. Abra seu navegador e digite a URL:

```
http://localhost:3000
```

Pronto, agora é só conversar com o bot :)
